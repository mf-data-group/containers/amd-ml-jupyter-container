# amd-ml-jupyter-container

A basic container setup for Machine Learning on AMD GPUs

## Run from command line docker as follows:

```
docker run --name amd-ml-jupyter -d \
           --restart=always \
           --device=/dev/kfd --device=/dev/dri --group-add video \
           -p <SERVER PORT TO LISTEN ON>:8888 -p 9515:9515 \
           --cap-add=SYS_ADMIN \
           -e JUPYTER_ENABLE_LAB=yes \
           --mount type=bind,source=/home/<USERNAME>/work,target=/home/jovyan/work \
           registry.gitlab.com/mf-data-group/containers/amd-ml-jupyter-container:latest \
           start-notebook.sh \
           --NotebookApp.password=<OUTPUT FROM `ipython -c "from jupyter_server.auth import passwd; passwd()"`>
```
