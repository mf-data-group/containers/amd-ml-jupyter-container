FROM quay.io/jupyter/datascience-notebook
# Get the latest image tag at:
# https://hub.docker.com/r/jupyter/datascience-notebook/tags/
# Inspect the Dockerfile at:
# https://hub.docker.com/r/jupyter/datascience-notebook/dockerfile

# install additional package...
# bio packages compiled from source or downloaded as binaries will live in the /bio directory
USER root
# install additional packages for prereqs
RUN apt-get update
RUN apt-get install -yq --no-install-recommends \
    asciinema \
	bc \
	build-essential \
	chromium-browser \
	csh \
	curl \
	default-jdk \
	firefox \
	git \
	graphviz \
	hashdeep \
	btop \
	jq \
	less \
	parallel \
	postgresql \
	postgresql-client \
	rename \
	rsync \
	screen \
	ssh \
	tmux \
	tree \
	vim \
	wget \
	xvfb \
	zip 
	
RUN curl https://rclone.org/install.sh | bash

USER $NB_UID
ENV CONDA_ENVS=/opt/conda/envs

# Here I'm just installing directly to the base conda installation
RUN mamba install \
    -c anaconda \
    -c bioconda \
    -c bokeh \
    -c conda-forge \
    -c plotly \
    papermill \
    bash_kernel \
    biopython \
    bokeh \
    bottleneck \
    colorlover \
    dash \
    dask-labextension \
	datashader \
	exspy \
    geckodriver \
    gitpython \
    hdbscan \
    humanize \
	jupyterlab-git \
    jupyter-server-proxy \
    plotly \
    plotly-orca \
    pytables \
    selenium \
    dask \
    distributed \
    netCDF4 \
    umap-learn \
    xarray \
    xmltodict 

# the pyviz package repo is much better at being up to date
RUN mamba install \
    -c pyviz \
    holoviews \
    hvplot \
    panel \
    pyviz_comms \
    markdown

# the following either can't be found in conda or are severely out of date
RUN pip install \
	exif \
	hyperspy_gui_ipywidgets \
	jupyterthemes \
	opencv-python \
	imutils \
	graphviz \
	ratarmount \
	sh \
	h5netcdf
  
# numpy >= 2 seems to break things
RUN pip install "numpy<2"

RUN jt -t grade3 -cellw 80% -ofs 11 -dfs 10 -fs 11 -tfs 11 -nfs 11 -mathfs 11

# install rcom pytorch
RUN pip3 install --pre torch torchvision torchaudio --index-url https://download.pytorch.org/whl/nightly/rocm5.6/
